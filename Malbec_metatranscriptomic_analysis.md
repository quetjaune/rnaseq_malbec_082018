Bioinformatic guide for metatranscriptomic analysis of *Vitis vinifera* cv. Malbec
================================================================================


Schematic summary of the analysis
---------------------------------

![schema_bioinf_analysis](https://drive.google.com/uc?export=view&id=1RMPIrigig6T1MN9SRlLbs4WXK2orx52b)

Quality filtering
-----------------
### Through FastQC ###
If required download Fastqc (linux version) from https://www.bioinformatics.babraham.ac.uk/projects/download.html
run analysis
~~~
for f in ../../../raw_data/*fastq.gz; do /usr/src/FastQC/fastqc $f --extract --outdir=/home/quetjaune/Documents/RNAseq_results_06082018/raw_data/1_quality_check/fastqc_out/; done
~~~
### Through trimmomatic ###
Download version 0.38 desde http://www.usadellab.org/cms/?page=trimmomatic
run analysis
```
for f in $(ls *fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); do java -jar /usr/src/Trimmomatic-0.38/trimmomatic-0.38.jar PE -threads 8 -basein ${f}_1.fastq.gz -baseout ./1_quality_check/trimmomatic_out/${f}.fastq.gz ILLUMINACLIP:/usr/src/Trimmomatic-0.38/adapters/TruSeq3-PE-2.fa:2:30:10 SLIDINGWINDOW:4:15 LEADING:5 TRAILING:5 MINLEN:25; done
```
Grapevine (Host) Gene Expression
--------------------------------
### 1) Mapping on grapevine genome and count unique mapping reads using STAR, fuse the count ouput for each sample to construct the count matrix ###
Install STAR2.6.1b through conda:
```
conda install -c bioconda star 
```
Download the required genome files from ENSEMBL FTP: [assembly.fasta](ftp://ftp.ensemblgenomes.org/pub/release-41/plants/fasta/vitis_vinifera/dna/) and [annotation.gtf](ftp://ftp.ensemblgenomes.org/pub/release-41/plants/gtf/vitis_vinifera)
```
wget ftp://ftp.ensemblgenomes.org/pub/release-41/plants/gtf/vitis_vinifera/Vitis_vinifera.12X.41.chr.gtf.gz
wget ftp://ftp.ensemblgenomes.org/pub/release-41/plants/fasta/vitis_vinifera/dna/Vitis_vinifera.12X.dna.toplevel.fa.gz
```
Uncompress
```
gunzip *.gz
```
Prepare the index
```
STAR --runThreadN 48 --runMode genomeGenerate --genomeDir ./ --genomeFastaFiles Vitis_vinifera.12X.dna.toplevel.fa --sjdbGTFfile Vitis_vinifera.12X.41.chr.gtf
```
Run mapping 
```
for f in $(ls *[1-2].fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); 
do STAR --readFilesCommand zcat --runThreadN 20 --genomeDir ../genome_star_indexed/ --readFilesIn ${f}_1.fastq.gz ${f}_2.fastq.gz --outSAMtype BAM Unsorted --quantMode TranscriptomeSAM GeneCounts --outFileNamePrefix ${f} --outReadsUnmapped Fastx;
done
```
Construct the count matrix in R (build_count_matrix.R)
```
ff <- list.files( path = "./counts", pattern = "*ReadsPerGene.out.tab$", full.names = TRUE )
counts.files <- lapply( ff, read.table, skip = 4 )
counts <- as.data.frame( sapply( counts.files, function(x) x[ , number ] ) )
ff <- gsub( "[.]ReadsPerGene[.]out[.]tab", "", ff )
ff <- gsub( "[.]/counts/", "", ff )
colnames(counts) <- ff
row.names(counts) <- counts.files[[1]]$V1
```
### 2)Evaluate gene expression and functional enrichment through edgeR-GOseq-GOplot pipeline ###
Using [edgeR_gene_expression_analysis.md](https://bitbucket.org/quetjaune/rnaseq_malbec_082018/src/master/edgeR_gene_expression_analysis.md) and [GOplot.R](https://bitbucket.org/quetjaune/rnaseq_malbec_082018/src/d597694efc1043ce1f702e19075b6f82dbc1d123/GOplot.R) files


Organelle and plant rRNA remotion (Despite the libraries were constructed using plant ribozero kit, still remain plant rRNA reads in output file)
-------------------------------------------------------------------------------------------------------------------------------------------------
Using the most representative sequence obtained in previous run MATAM output (rRNA from A. thaliana) as query in NCBI blast, I select the best 100 hits complete sequences and download it, then regions containing Ns were removed through sed and the resulting rRNA_NCBI.fasta file was used to filter out
Concatenate the rRNA sequences and organelle-derived sequences
```
cat rRNA_ncbi.fasta all_organelle_ADN_ARN.fasta > rRNA_organelle.fasta
```
Remove the mapped reads
```
STARWD="../raw_grpvne_reads/trimmomatic_out/"
for f in $(ls $STARWD*Unmapped.out.mate[1-2] | cut -f4 -d"/" | sed 's/Unmapped.out.mate[1-2]//' | uniq); do bowtie2 -p 30 -x rRNA_organelle -1 $STARWD${f}Unmapped.out.mate1 -2 $STARWD${f}Unmapped.out.mate2 | samtools view -bS -f 4 - | sam
tools fastq - > ${f}_rrna_org_unmap.fq ; done
```
Prokaryotic and fungal microbiome characterization through MATAM
--------------------------------------------------------------
Install MATAM
```
conda install matam
```

### For prokaryotic microbiome the [SILVA-SSU-DB](link?) is required ###

Prepare SILVA-SSU-DB reference for MATAM
```
matam_db_preprocessing.py -i SILVA_132_SSURef.fasta --cpu 30 --max_memory 128000
```
Run MATAM assembly for all the samples
```
BWTIEDIR="../bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f3 -d"/" | sed 's/_rrna_org_unmap.fq//g'); do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d SILVA_132_SSURef_edit_NR95 -o ./${f}_matam_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200; done
```
Construct the table for comparisons between samples
```
1) construir el archivo para indicar paths de muestras para archivos fasta y rdp, terminar de editar detalles con nano
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do echo $d $d*{fa,tab} | sed 's/ /\t/g' >> samples_file; done
2) Correr comparacion con matam
matam_compare_samples.py -s samples_file -t abundance_table.txt -c taxonomy_table.txt
```
Modification of output for compatibility with phyloseq R script:
```
for q in "c22" "c23" "c25" "m11" "m16" "m26"; do grep "$q" abundance_table.txt | cut -f1-4 > ${q}_raw_abun; done
# then the sum of all the counts per sample is required:
for f in "c22" "c23" "c25" "m11" "m16" "m26"; do cut -f3 ${f}_raw_abun | awk '{sum+=$1} END {print sum}'; done
#RESULT
#c22 110995
#c23 199276
#c25 161955
#m11 125936
#m16 141967
#m26 156236
#finally is required to modify the taxonomy_table.txt file to express the values of raw abunance instead of normalized values and separate the taxonomy file from otu table files
sed 's/None/0.00001/g' taxonomy_table.txt > tax_table_with0.txt
gawk -F$'\t' 'BEGIN {OFS = FS} $2*=1109.95' tax_table_with0.txt | gawk -F$'\t' 'BEGIN {OFS = FS} $3*=1992.76' | gawk -F$'\t' 'BEGIN {OFS = FS} $4*=1619.55' | gawk -F$'\t' 'BEGIN {OFS = FS} $5*=1259.36' | gawk -F$'\t' 'BEGIN {OFS = FS} $6*=1419.67' | gawk -F$'\t' 'BEGIN {OFS = FS} $7*=1562.36' > tax_table_raw_abun.txt
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1,3-8 > otu_table_4_phyloseq.tab
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1-2 |  sed -s 's/;/\t/g' > tax_table_4_phyloseq.tab 
#final editing with nano to include ("otu	Domain	Supergroup	Division	Class	Order Family	Genus	Species") as header in tax file and ("otu	c22	c23	c25	m11	m16	m26") as header in otu file
```
### FUNGAL microbiome is required to download [WARCUP_RDP_ITS_DB](https://www.drive5.com/sintax/rdp_its_v2.fa.gz). Also need to use the rdp.patch in rdp.py to allow the classification ###
Prepare RDP-ITS-DB to be uses as reference in MATAM
```
matam_db_preprocessing.py -i rdp_its_v2.fa -m 200 -n 2 --clustering_id_threshold 0.97 --cpu 30 --max_memory 128000 --verbose
```
Run MATAM
```
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d rdp_its_v2_NR97 -o ./${f}_RDPITS_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungalits_warcup; 
done
```

For Sample comparison, is required to join in a folder in OMICA the **rdp** and **fasta** files and then run **matam_compare_samples.py**
```
1) construir el archivo para indicar paths de muestras para archivos fasta y rdp, terminar de editar detalles con nano
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do echo $d $d*{fa,tab} | sed 's/ /\t/g' >> samples_file; done
2) Correr comparacion con matam
matam_compare_samples.py -s samples_file -t abundance_table.txt -c taxonomy_table.txt
3)Download output files from OMICA
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/rdp_warcup_db/matam_sampple_comparison/abundance_table.txt ./
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/rdp_warcup_db/matam_sampple_comparison/taxonomy_table.txt ./
4) Sum the total counts per sample
for q in "c22" "c23" "c25" "m11" "m16" "m26"; do grep "$q" abundance_table.txt | cut -f1-4 > ${q}_raw_abun; done
# then the sum of all the counts per sample is required:
for f in "c22" "c23" "c25" "m11" "m16" "m26"; do cut -f3 ${f}_raw_abun | awk '{sum+=$1} END {print sum}'; done
#RESULT
#c22 1099
#c23 3622
#c25 1302
#m11 1600
#m16 1337
#m26 965
#finally is required to modify the taxonomy_table.txt file to express the values of raw abunance instead of normalized values and separate the taxonomy file from otu table files
sed 's/None/0.00001/g' taxonomy_table.txt > tax_table_with0.txt
gawk -F$'\t' 'BEGIN {OFS = FS} $2*=10.99' table_with0.txt | gawk -F$'\t' 'BEGIN {OFS = FS} $3*=36.22' | gawk -F$'\t' 'BEGIN {OFS = FS} $4*=13.02' | gawk -F$'\t' 'BEGIN {OFS = FS} $5*=16' | gawk -F$'\t' 'BEGIN {OFS = FS} $6*=13.37' | gawk -F$'\t' 'BEGIN {OFS = FS} $7*=9.65' > tax_table_raw_abun.txt
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1,3-8 > otu_table_4_phyloseq.tab
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1-2 |  sed -s 's/;/\t/g' | sed 's/ /_/g' > tax_table_4_phyloseq.tab 
#final editing with nano to include ("otu     Superphylum     Phylum  Class   Order Family    Genus	Species") as header in tax file and ("otu	c22	c23	c25	m11	m16	m26") as header in otu file
```
"Whole" microbiome characterization using KrakenUniq
----------------------------------------------------

Download [krakenuniq](https://github.com/fbreitwieser/krakenuniq) 
Download the indexed database containing all the NCBI genomes from microbes (archeas, bacteria, virus and fungi) in NT DB (around 240 GB). (IS absolutely required HPC)
```
wget -c ftp://ftp.ccb.jhu.edu/pub/software/krakenuniq/Databases/nt/*
```
first prepare hte file to be used as reference
```
#krakenuniq-build --db microb_nt_db --kmer-len 31 --threads 10 --taxids-for-genomes --taxids-for-sequences --jellyfish-hash-size 6400M
```
run KrakenUniq
```
krakenuniq --db microb_nt_db --fastq-input ../bowtie2_out/c22_rrna_org_unmap.fq --threads 12 --report-file REPORTFILE.tsv > READCLASSIFICATION.tsv
```
Then to contruct Krona graphs, first we have to update all the files regarding taxonomy or accesions IDs through **updateTaxonomy.sh** and **updateAccession.sh?**, later
 from Krakenuniq output (REPORTFILE.tsv) the next command was used for each sample:
```
ktImportTaxonomy -o m26_s6_kraken.krona.html -t 7 -s 6 REPORTFILE.tsv 
```

Microbial gene expression and functional pathway analysis
---------------------------------------------------------
Download **[Humann2](https://bitbucket.org/biobakery/humann2/wiki/Home)**
Create environment **humann** (because of python2.7 requirement), Install through conda and activate first to run in OMICA
```
conda create -n humann python=2.7 anaconda
conda install -c bioconda humann2
source activate humann
```
Run humann2 for all the samples
```
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do humann2 --input ../bowtie2_out/$f_rrna_org_unmap.fq --bypass-prescreen --threads 12 --output $f;
done
```
Alternatively we could run [metaphlan](https://bitbucket.org/biobakery/metaphlan2/) first, to identify the taxonomic profile (based only on marker genes) for all the samples, 
and then use the list of identified microorganisms to continue with humann2 pipeline:
```
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR[cm][1,2][1,3,5,6]_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do metaphlan2.py $BWTIEDIR${f}_rrna_org_unmap.fq --mpa_pkl /LUSTRE/bioinformatica_data/genomica_funcional/paoline/anaconda3/envs/humann/bin/databases/mpa_v20_m200.pkl --bowtie2db /LUSTRE/bioinformatica_data/genomica_funcional/paoline/anac
onda3/envs/humann/bin/databases/ --nproc 12 --input_type fastq > ${f}_profiled_metagenome.txt;
done
```
And then run humann2
```
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do humann2 --input ../bowtie2_out/${f}_rrna_org_unmap.fq --taxonomic-profile ${f}_profiled_metagenome.txt --threads 12 --output ${f};
done
```
join tables of each sample and graph barplot images
```
humann2_join_tables -i ./ -o all_join_tables.tsv
humann2_barplot -i control_join_tables_RPK.tsv -f UniRef50_A0A009PBA3 -o controls_A0A009PBA3
```
Normalize counts (make the data independent of the bias introduced by different sequencing deep and allow the correct sample comparison), regroup based on EC codes (from KEGG)
```
humann2_renorm_table --input all_join_tables_RPK.tsv --output all_join_genefamilies_RPKs_cpm.tsv --units cpm --update-snames
humann2_regroup_table -i all_join_genefamilies_RPKs_cpm.tsv -o all_join_level4ec-cpm.tsv -g uniref50_rxn
```
Make statistical analysis (Kluscal-Wallis) after modifying **all_join_level4ec-cpm.tsv** file with a new row that indicates the symptomatic and asymptomatic samples 
```
humann2_associate --input all_join_level4ec-cpm.tsv --last-metadatum SYMPT --focal-metadatum SYMPT --focal-type categorical -o stats_SYM_vs_ASYM.txt -f 0.5
```