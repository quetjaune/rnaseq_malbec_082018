## Cargando los paquetes necesarios en R ##
```
library("limma")
```
*if required: install.packages("limma")*
```
library("edgeR")
```
*if required: install.packages("edgeR")*
```
library(rgl)
```
*If required, install: biocLite("rgl")*
Otras librerias necesarias
```
library(shiny)
library(heatmaply)
library(shinyHeatmaply)
```
*If required, install ggplot2*
```
devtools::install_github('hadley/ggplot2')
library(ggplot2)
library(gplots)
```
*If required: biocLite(gplots)*
```
library(RColorBrewer)
```
*If required: install.packages("RColorBrewer")*

*for functional enrichment **goseq 1.32.0**, **qvalue 2.10.0** are required*

```
source("https://bioconductor.org/biocLite.R")
biocLite("goseq")
biocLite("qvalue")
biocLite("GO.db")
```
to load packages
```
library(goseq)
library(qvalue)
library(GO.db)
```
*para revisar la versiones de R y los paquetes instalados usar:sessionInfo()*

## Set working directory and load count table (count_table.tab) that was obtained through mapping on (Pinot Noir) grpaevine genome ##
```
setwd("/home/quetjaune/Documents/RNAseq_results_06082018/raw_data/2_grapevine_mapping/star_out/edgeR_out")
```

### **1. Load count matrix** ###
```
counts = read.table("../count_table.tab", header=TRUE, row.names = 1, sep="\t", comment.char="") 
dim(counts)
```
Check the structure of the file
```
head(counts)
dim(counts)
```
Filtering the genes having 2 counts per millon (cpm) at least in 2 samples
```
counts = counts[rowSums(cpm(counts) >= 2) >=2,]
```
Check the structure of the file
```
dim(counts)
```
Normalization
```
normalizados= cpm(counts)
head(normalizados)
dim(lognormalizados)
```
Group the samples based on replicates
```
head(counts)
head(normalizados)
counts =counts[,c(2:3,4,6)]
grp = sub("..$", "", colnames(counts))
grp
dge = DGEList(counts=counts, group = grp) 
dge
```
Graph multidimensiona sclaing (MDS) plots:
```
pdf("MDSplot.pdf")
cols <- rep(c("red","green"), each = 2)
plotMDS(dge, top=500, col=cols, xlab="Dim1", ylab="Dim2", dim.plot=c(1,2))
abline(h= c(1,-1),  col="blue", lwd=1, lty=2)
abline(v= c(1,-1),  col="blue", lwd=1, lty=2)
dev.off()
```
Graph a heatmap plot
```
var_genes <- apply(log(cpm_counts), 1, var)
head(var_genes)
select_var <- names(sort(var_genes, decreasing=TRUE))[1:500]
head(select_var)
length(select_var)
highly_variable_lcpm <- log(cpm_counts)[select_var,]
head(highly_variable_lcpm)
#### Definimos algunos colores interesantes
mypalette <- brewer.pal(11,"RdYlBu")
morecols <- colorRampPalette(mypalette)

#### Imprimir heatmap como imagen
png(file="High_var_genes.heatmap.png")
heatmap.2(as.matrix(highly_variable_lcpm),col=rev(morecols(50)),trace="none", main="Top 500 most variable genes across samples",scale="row")
dev.off()
```
## **2. Dispersion data calculation and identification of genes expressed differentially in SYM vs ASYM** ##

```
design = model.matrix(~0 + dge$samples$group)
colnames(design) = levels(dge$samples$group)
design
```
Data dispersion calculation based on general linear model
```
dge = estimateGLMCommonDisp(dge, design= design)
dge = estimateGLMTrendedDisp(dge, design= design)
dge = estimateGLMTagwiseDisp(dge, design= design)
```
Graph Biological Coefficient of Variation
```
pdf("BCVplot.pdf")
plotBCV(dge)
dev.off()
```
Using the previous calculated dispersion, fit to GLM
```
fit=glmFit(dge, design) 
```
Set the comparisons to be done
```
contVector <- c(
  "malvón_vs_control"= "m-c"
  )
contMatrix <- makeContrasts(contrasts=contVector, levels=design)
colnames(contMatrix) <- names(contVector)
```
Create and specify the output directory
```
outpathcount = "./contrasts/"
dir.create(outpathcount, showWarnings=FALSE)
```
### Generate the comparison matrix and get the list of differential expressed genes ###
```
for (comp in colnames(contMatrix)) {
  print(comp)
  
  lrt <- glmLRT(fit, contrast=contMatrix[,comp])
  
  topTab <- topTags(lrt, n=Inf)$table
  
  deGenes <- rownames(topTab)[topTab$FDR < 0.05] #& abs(topTab$logFC) > 0]
  deGenes_up <- rownames(topTab)[topTab$FDR < 0.05 & (topTab$logFC) > 0]
  deGenes_down <- rownames(topTab)[topTab$FDR < 0.05 & (topTab$logFC) < 0]
  
  print (length(deGenes))
  print (length(deGenes_up))
  print (length(deGenes_down))
}
```
to generate Smearplot
```
for (comp in colnames(contMatrix)) {
  print(comp)
    lrt <- glmLRT(fit, contrast=contMatrix[,comp])
    topTab <- topTags(lrt, n=Inf)$table
    deGenes <- rownames(topTab)[topTab$FDR < 0.01] #& abs(topTab$logFC) > 0]
  deGenes_up <- rownames(topTab)[topTab$FDR < 0.01 & (topTab$logFC) > 2]
  deGenes_down <- rownames(topTab)[topTab$FDR < 0.01 & (topTab$logFC) < 2]
pdf(comp, onefile=FALSE)
plotSmear(lrt, de.tags=deGenes, main=comp)
abline(h= c(1,-1), col="blue", lwd=1, lty=2)
dev.off()
}
```
to obtain the list of degs
```
for (comp in colnames(contMatrix)) {
  print(comp)
    lrt <- glmLRT(fit, contrast=contMatrix[,comp])
    topTab <- topTags(lrt, n=Inf)$table
    deGenes <- rownames(topTab)[topTab$FDR < 0.05] #& abs(topTab$logFC) > 0]
  deGenes_up <- rownames(topTab)[topTab$FDR < 0.05 & (topTab$logFC) > 0]
  deGenes_down <- rownames(topTab)[topTab$FDR < 0.05 & (topTab$logFC) < 0]
  write.table(topTab, file=paste(outpathcount, comp, ".txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_up, file=paste(outpathcount, comp, "_up.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_down, file=paste(outpathcount, comp, "_down.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
}
```


### **3. Functional enrichment based on the list of differential expressed genes** ###

GOseq para obtener enriquecimiento funcional, descargar archivos con la asignación de ontologia, longitud de transcriptos e indicación de contrastes 
####Ejemplo con cluster 1. Primero designamos la lista que queremos inspeccionar en el enriquecimiento
Load the list of genes
Load the gene lengths
```
gene_lengths = read.table("./functional_enrichment/gene_lengths", row.names=1, header=T)
rownames(gene_lengths)
head(gene_lengths)
dim(gene_lengths)
```
Load the ID for all the genes from grapevine genome
```
background = scan("./functional_enrichment/all_IDs", what="", sep="\n")
length(background)
background.gene_ids = rownames(gene_lengths)
head(background)
length(background.gene_ids)
head(background.gene_ids)
length(deg_cl1)
head(deg_cl1)
sample_set_gene_ids=deg_cl1
sample_set_gene_ids = unique(c(background.gene_ids,deg_cl1))
head(sample_set_gene_ids)
length(sample_set_gene_ids)
```
Load the GO assignments for each gene in grapevine genome
```
GO_info = read.table("./functional_enrichment/go_assignments", row.names = 1)
GO_info_listed = apply(GO_info, 1, function(x) unlist(strsplit(x,',')))
names(GO_info_listed) = rownames(GO_info)
```
Create the function for gene ontology assignment
```
get_GO_term_descr =  function(x) {
  d = 'none';
  go_info = GOTERM[[x]];
  if (length(go_info) >0) { d = paste(Ontology(go_info), Term(go_info), sep=' ');}
  return(d);
}
```
Run GOseq
####Asignar términos GO a la lista de genes de interes
```
GO_to_gene_list = list()
for (gene_id in intersect(names(GO_info_listed), sample_set_gene_ids)) {
    go_list = GO_info_listed[[gene_id]]
    for (go_id in go_list) {
        GO_to_gene_list[[go_id]] = c(GO_to_gene_list[[go_id]], gene_id)
    }
}
```
####check dimensions
```
head(GO_to_gene_list)
length(GO_to_gene_list)
```
####check if all the genes have assiggnments of gene length
```
missing_gene_lengths = sample_set_gene_ids[! sample_set_gene_ids %in% rownames(gene_lengths)]
if (length(missing_gene_lengths) > 0) {
  stop("Error, missing gene lengths for features: ", paste(missing_gene_lengths, collapse=', '))
}
```
###Run GOseq analysis
```
sample_set_gene_lengths = gene_lengths[sample_set_gene_ids,]
GO_info_listed = GO_info_listed[ names(GO_info_listed) %in% sample_set_gene_ids ]
length(cat_genes_vec)
head(cat_genes_vec)
length(gene_lengths)
head(DE_genes_common)
length(DE_genes_common)
DE_genes_bin=as.integer(rownames(gene_lengths)%in%deg_cl1)
head(DE_genes_bin)
length(DE_genes_bin)
gene_lengths=gene_lengths[,1]
head(gene_lengths)
length(gene_lengths)
dim(gene_lengths)
pwf=nullp(DE_genes_bin, bias.data=gene_lengths[,1])
head(pwf)
dim(pwf)
rownames(pwf) = rownames(gene_lengths)
    res = goseq(pwf,gene2cat=GO_info_listed, use_genes_without_cat=TRUE)
```
#### print tables for overrepressented categories:
  ```
     pvals = res$over_represented_pvalue
     pvals[pvals > 1 - 1e-10] = 1 - 1e-10
     q = qvalue(pvals)
     res$over_represented_FDR = q$qvalues
    go_enrich_filename = paste('deg_vs_all.GOseq.enriched', sep='')
    result_table = res[res$over_represented_pvalue<=0.05,]
    descr = unlist(lapply(result_table$category, get_GO_term_descr))
    write.table(result_table[order(result_table$over_represented_pvalue),], file=go_enrich_filename, sep='	', quote=F, row.names=F)
```
#### print tables for underrepressented categories:
    ```
     pvals = res$under_represented_pvalue
     pvals[pvals>1-1e-10] = 1 - 1e-10
     q = qvalue(pvals)
     res$under_represented_FDR = q$qvalues
    go_depleted_filename = paste('deg_vs_all.GOseq.depleted', sep='')
    result_table = res[res$under_represented_pvalue<=0.1,]
    descr = unlist(lapply(result_table$category, get_GO_term_descr))
    result_table$go_term = descr;
    write.table(result_table[order(result_table$under_represented_pvalue),], file=go_depleted_filename, sep='	', quote=F, row.names=F)
```
###INFORMACIÓN DEL AMBIENTE
```
sessionInfo()
```
